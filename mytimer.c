/**

  * @file timer.c
 
  * @author Franz Korf, HAW Hamburg 
 
  * @date Januar 2017
 
  * @brief Dieses Modul geh�rt zur L�sung der GS Klausur WS 2016-17. 
 
  */
 

#include "timer.h"

#include "TI_memory_map.h"



#define TIM2_CLOCK      84  // [Mhz]. 84 Timer Ticks == 1 us

/**
 * @brief Diese Funktion wartet die vorgegebene Zeitspanne.
 *        Sie ist durch polling der Timers TIM2 realisiert.
 * @param t Die Zeit in us, die gewartet werden soll. Der Wertebereich des 
 *        Parameters ist 0 .. (UINT32_MAX-1)/TIM2_CLOCK
 */
void sleep(const uint32_t t) {
    uint32_t wait_timestamp = getTimeStamp() + t * TIM2_CLOCK;
    while (getTimeStamp() < wait_timestamp);
    
    /*uint32_t current_timestamp = getTimeStamp();
    if (UINT32_MAX - t * TIM2_CLOCK < current_timestamp) { // overflow
        current_timestamp + t * TIM2_CLOCK;
    } else {
        while (getTimeStamp() < current_timestamp + t * TIM2_CLOCK);
    }*/
}


// EOF

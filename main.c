/**
 * @file main.c
 * @author Franz Korf, HAW Hamburg 
 * @date Dezember 2016
 * @brief Dieses Modul geh�rt zur L�sung der GS Probeklausur WS 2016-17. 
 */

/* Includes ------------------------------------------------------------------*/
//#include "main.h"
#include "TI_Lib.h"
#include "mytimer.h"
#include "timer.h"
#include "myerr.h"
#include "TFTOutput.h"
#include "hal.h"
#include "general.h"
#include "input.h"

/*
 *          T E S T  F U N K T I O N E N
 */
 
///*
// * @brief Einfacher Test des TFToutput Moduls
// *        Belegung der LEDs.
// */
void testTFToutput(void) {
	 Init_TI_Board();
	 initTerm();
	 resetError();

   druckeOperationMitErgebnis(MULT_OP, 7, 6, 42);
	 // Jetzt sollte auf dem TFT Display folgender Text stehen: 7 * 6 = 42
   Delay(4000);	
	
	 druckeOperation(PLUS_OP, 2, 1);
	 // Jetzt sollte auf dem TFT Display folgender Text stehen: 2 + 1 = ?
	 Delay(4000);
	
	 setError(IO_ERR);
	 druckeError();
	 // Jetzt sollte auf dem Display folgender Text stehen: Error : 2 
   Delay(4000);

	 druckeOperationMitErgebnis(SUB_OP, 1, 3, -2);
	 // Jetzt sollte auf dem TFT Display folgender Text stehen: 1 - 3 = -2
}

void testTimer(void) {
	 Init_TI_Board();
	 initTerm();
	 resetError();
	 initTimer();
	
	 sleep(3*1000*1000);
	 // zwischen den beiden Textzeilen sollen ca. 15 Sekunden vergehen.
	 druckeOperation(PLUS_OP, 2, 1);
	 sleep(15*1000*1000);
   druckeOperationMitErgebnis(PLUS_OP, 2, 1, 3);

	 // zwischen den beiden Textzeilen sollen ca. 5 Sekunden vergehen.
	 // Hier wird der Overflow bei Zeitstempeln getestet.
	 while (getTimeStamp() < (0xFFFFFFFF - 84*1000*1000 * 3)) { };
	 druckeOperation(MULT_OP, 2, 5);
   sleep(5*1000*1000);
   druckeOperationMitErgebnis(MULT_OP, 2, 5, 10);
}

void testHal(void){
	 Init_TI_Board();
	 initTerm();
	 resetError();
	 initTimer();
	
	 // teste getSteigendeFlanken
	 // Die Ausgabe sollte -x------ auf LED_GRP_2
	 setOutputOfPort(LED_PORT, LED_GRP_2, getSteigendeFlanken(0xA000,0xC000)); 
	 sleep(5000*1000);
	 // teste kippeMaskierteBits
	 // Die Ausgabe sollte -----x-x auf LED_GRP_2
	 uint16_t v = 0x0900;
	 kippeMaskierteBits(0x0C00,&v);
	 setOutputOfPort(LED_PORT, LED_GRP_2, v);
	 sleep(5000*1000);	 
	 
	 // Die blauen LEDS D21 bis D28 erzeugen folgendes Ausgabemuster im Zeitabstand von 1s
	 // x------x
	 // x--xx--x
	 // xx-xx-xx
	 // x-x-x-x-
	 
	 setOutputOfPort(LED_PORT, LED_GRP_2, 0x8100);
	 sleep(1000*1000);
	 setOutputOfPort(LED_PORT, LED_GRP_2, 0x9900);
	 sleep(1000*1000);
	 setOutputOfPort(LED_PORT, LED_GRP_2, 0xDB00);
	 sleep(1000*1000);
	 setOutputOfPort(LED_PORT, LED_GRP_2, 0xAA00);
	
   // Die blauen LEDS D13 bis D20 erzeugen folgendes Ausgabemuster im Zeitabstand von 1s	
	 // x------x
	 // x--xx--x
	 // xx-xx-xx
	 // x-x-x-x-
	 setOutputOfPort(LED_PORT, LED_GRP_1, 0x81);
	 sleep(1000*1000);
	 setOutputOfPort(LED_PORT, LED_GRP_1, 0x99);
	 sleep(1000*1000);
	 setOutputOfPort(LED_PORT, LED_GRP_1, 0xDB);
	 sleep(1000*1000);
	 setOutputOfPort(LED_PORT, LED_GRP_1, 0xAA);

	
	 // Warte auf den Tastendruck von S3 und geben 0x03 auf LED_GRP_1 aus.
	 uint16_t oldSchalterVal = readButtons(SCHALTER_PORT, SCHALTER_GRP);
	 uint16_t newSchalterVal = readButtons(SCHALTER_PORT, SCHALTER_GRP);
	 
	 const uint16_t schalter3 = 0x0008;
	 const uint16_t schalter0 = 0x0001;
   
	 // Warte, bis eine steigende Flanke an S3 erkannt wurde
	 while (schalter3 != (schalter3 & getSteigendeFlanken(oldSchalterVal, newSchalterVal))) {
	    oldSchalterVal = newSchalterVal;
	    newSchalterVal = readButtons(SCHALTER_PORT, SCHALTER_GRP);
	 } 
	 setOutputOfPort(LED_PORT, LED_GRP_1, 0x03);
	 
	 // Warte erneut auf den Tastendruck von S3 und geben 0x30 auf auf LED_GRP_1  aus.
	 oldSchalterVal = newSchalterVal;
	 newSchalterVal = readButtons(SCHALTER_PORT, SCHALTER_GRP);

	 while (schalter3 != (schalter3 & getSteigendeFlanken(oldSchalterVal, newSchalterVal))) {
	    oldSchalterVal = newSchalterVal;
	    newSchalterVal = readButtons(SCHALTER_PORT, SCHALTER_GRP);
	 }		 
   setOutputOfPort(LED_PORT, LED_GRP_1, 0x30);

	 // Warte bis S0 gedrueckt und anschlie�end wieder losgelassen wird.
	 // Gebe danach 0x0F auf  LED_GRP_1 aus.
	 // Warte auf sei
	 while (schalter0 != (schalter0 & getSteigendeFlanken(oldSchalterVal, newSchalterVal))) {
	    oldSchalterVal = newSchalterVal;
	    newSchalterVal = readButtons(SCHALTER_PORT, SCHALTER_GRP);
	 }
   // Warte darauf, dass S0 wieder losgelassen wird
   while (schalter0 == (schalter0 & readButtons(SCHALTER_PORT, SCHALTER_GRP))) { };
	 setOutputOfPort(LED_PORT, LED_GRP_1, 0x0f);
}

/*
 * F u n k t i o n e n  f u e r  d i e  A n w e n d u n g
 */



// EOF

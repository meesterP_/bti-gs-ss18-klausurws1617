/**
 * @file TFToutput.c
 * @author Franz Korf, HAW Hamburg 
 * @date Januar 2017
 * @brief Dieses Modul gehoert zur Loesung der GS Klausur WS 2016-17. 
 *        Es implementiert eine einfache terminalaehnliche Ansteuerung des 
 *        TFT Displays
 */
	
#include "TFTOutput.h"
#include <stdlib.h>
#include <stdio.h>
#include "general.h"
#include "myerr.h"
#include "tft.h"

// Definition of terminal area
#define TERM_FONT     2
#define TERM_POS_X    3
#define TERM_POS_Y    3
#define TERM_COLUMNS 44
#define TERM_LINES   14

#define OUT_STR_SIZE  64
	
/*
 ****************************************************************************************
 *  @brief Diese Funktion initialisiert des TFT Display. Ein einfaches Terminalfenster 
 *         wird auf dem TFT Display erzeugt.
 *
 ****************************************************************************************/
void initTerm(void) {
    TFT_Init();
}

/*
 ****************************************************************************************
 *  @brief Diese Funktion gibt den aktuellen Fehlercode auf dem TFT Display aus.
 *
 ****************************************************************************************/
void druckeError(void) {
    char error = getError();
    if (error != EOK) {
        TFT_cls();
        TFT_putc((char) error); 
    }
}

/*
 ****************************************************************************************
 *  @brief Diese Funktion gibt eine Operation mit Argumenten 
 *         in folgender Form auf dem TFT Display aus
 *            <linker Operand> <Operation> <rechter Operand>
 *  @param operation Die Operation. Der aktuelle Parameter ist eine entsprechende
 *                   Konstante aus general.h.
 *  @param leftOp Der linke Operand.
 *  @param rightOp Der rechte Operand.
 *
 ****************************************************************************************/
 void druckeOperation(char operation, unsigned char leftOp, unsigned char rightOp) {
     char text[32];
     snprintf(text, 32, "%d %c %d", leftOp, operation, rightOp);
     TFT_puts(text);
 }

/*
 ****************************************************************************************
 *  @brief Diese Funktion gibt eine Operation mit Argumenten und Ergebnis 
 *         in folgender Form auf dem TFT Display aus
 *            <linker Operand> <Operation> <rechter Operand> = <Ergebnis>
 *  @param operation Die Operation. Der aktuelle Parameter ist eine entsprechende
 *                   Konstante aus general.h.
 *  @param leftOp Der linke Operand.
 *  @param rightOp Der rechte Operand.
 *  @param erg Das Ergebnis der Berechnung.
 *
 ****************************************************************************************/
void druckeOperationMitErgebnis(char operation, unsigned char leftOp, unsigned char rightOp, signed char erg) {
    char text[32];
    snprintf(text, 32, "%d %c %d = %d", leftOp, operation, rightOp, erg);
    TFT_puts(text);
}

// EOF

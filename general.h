/**
 * @file general.h
 * @author Franz Korf, HAW Hamburg 
 * @date Januar 2017
 * @brief Dieses Modul geh�rt zur L�sung der GS Klausur WS 2016-17. 
 *        Es stellt die grundlegenden Konstanten und IO Einstellungen bereit.
 */

#ifndef _GENERAL_H
#define _GENERAL_H

#include "TI_memory_map.h"

// Eingabeport fuer die Schalter
#define SCHALTER_PORT        GPIOE
#define SCHALTER_GRP         0x00FF

// Position der Bits der Schalter im SCHALTER_PORT
#define S0    (0x0001 << 0)
#define S1    (0x0001 << 1)
#define S2    (0x0001 << 2)
#define S3    (0x0001 << 3)
#define S4    (0x0001 << 4)
#define S5    (0x0001 << 5)
#define S6    (0x0001 << 6)
#define S7    (0x0001 << 7)

// Ausgabeport f�r die LEDs
#define LED_PORT        GPIOG
#define PIN_ERSTE_LED   8
#define ANZ_LEDS        8
#define LED_GRP_1       0x00FF
#define LED_GRP_2       0xFF00

// Konstanten zur Beschreibung der Operationen des 3-Bit Rechners
#define PLUS_OP    '+'
#define SUB_OP     '-'
#define MULT_OP    '*'
#define DIV_OP     '/'

#endif
// EOF

/**
 * @file hal.c
 * @author Franz Korf, HAW Hamburg 
 * @date Januar 2017
 * @brief Dieses Modul geh�rt zur L�sung der GS Klausur WS 2016-17. 
 */

#include "hal.h"
#include <stdint.h>
#include <stdbool.h>
#include "timer.h"
#include "TI_memory_map.h"
#include "myerr.h"

#define ZEITSPANNE_PRELLEN       (5*1000)     // us
#define ANZAHL_LESEVERSUCH       3            // Anzahl Leseversuche f�r die Verz�gerung beim Entprellen
  


// EOF

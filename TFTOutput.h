/**
 * @file TFToutput.h
 * @author Franz Korf, HAW Hamburg 
 * @date Januar 2017
 * @brief Dieses Modul geh�rt zur L�sung der GS Klausur WS 2016-17. 
 *        Es implementiert eine einfache terminalaehnliche Ansteuerung des 
 *        TFT Displays.
 */

#ifndef _TFTOUTPUT_H
#define _TFTOUTPUT_H

#include <stdint.h>
#include <stdbool.h>

/*
 ****************************************************************************************
 *  @brief Diese Funktion initialisiert des TFT Display. Ein einfaches Terminalfenster 
 *         wird auf dem TFT Display erzeugt.
 *
 ****************************************************************************************/
void initTerm(void);

/*
 ****************************************************************************************
 *  @brief Diese Funktion gibt den aktuellen Fehlercode auf dem TFT Display aus.
 *
 ****************************************************************************************/
void druckeError(void);

/*
 ****************************************************************************************
 *  @brief Diese Funktion gibt eine Operation mit Argumenten 
 *         in folgender Form auf dem TFT Display aus
 *            <linker Operand> <Operation> <rechter Operand>
 *  @param operation Die Operation. Der aktuelle Parameter ist eine entsprechende
 *                   Konstante aus general.h.
 *  @param leftOp Der linke Operand.
 *  @param rightOp Der rechte Operand.
 *
 ****************************************************************************************/
 void druckeOperation(char operation, unsigned char leftOp, unsigned char rightOp);

/*
 ****************************************************************************************
 *  @brief Diese Funktion gibt eine Operation mit Argumenten und Ergebnis 
 *         in folgender Form auf dem TFT Display aus
 *            <linker Operand> <Operation> <rechter Operand> = <Ergebnis>
 *  @param operation Die Operation. Der aktuelle Parameter ist eine entsprechende
 *                   Konstante aus general.h.
 *  @param leftOp Der linke Operand.
 *  @param rightOp Der rechte Operand.
 *  @param erg Das Ergebnis der Berechnung.
 *
 ****************************************************************************************/
void druckeOperationMitErgebnis(char operation, unsigned char leftOp, unsigned char rightOp, signed char erg);

#endif /* _TFTOUTPUT_H */

// EOF

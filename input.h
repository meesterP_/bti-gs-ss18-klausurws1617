/**
 * @file input.h
 * @author Franz Korf, HAW Hamburg 
 * @date Januar 2017
 * @brief Dieses Modul gehoert zur L�sung der GS Klausur WS 2016-17. 
 *        Es implementiert den Hilfsfunktionen zur Behandlung des Inputs.
 */
 
#ifndef _INPUT_H
#define _INPUT_H
 
#include <stdint.h>

 /**
 * @brief Diese Funktion vergleicht einen "alten" und einen "neuen" Registerwert
 *        und erkennt bitweise steigende Flanken. Eine steigende Flanke fuer Bit 
 *        i liegt vor, wenn der "alte" Wert von Bit i 0 ist und der "neue" Wert
 *        von Bit i 1 ist.
 * @param oldVal Der "alte" Wert des Registers.
 * @param newVal Der "neue" Wert des Registers.
 * @retval Die Bits, fuer die eine steigende Flanke vorliegt, sind gesetzt.
 */
uint16_t getSteigendeFlanken(uint16_t oldVal, uint16_t newVal);

/**
 * @brief Diese Funktion kippt die Bits seines call by reference Paramaters.
 *        Kippen bedeutet, dass ein Bit seinen Wert wechselt.
 *        Ein weiterer Parameter definiert, welche Bits gekippt werden sollen.
 * @param maske  definiert die Bits, die gekippt werden sollen.
 * @param val zeigt auf die 16 Bit Variable, dessen Bits gekippt werden sollen.
 */
void kippeMaskierteBits(uint16_t maske, uint16_t *val);

#endif
// EOF


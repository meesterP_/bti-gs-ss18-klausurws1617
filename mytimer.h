/**
 * @file timer.h
 * @author Franz Korf, HAW Hamburg 
 * @date Dezember 2016
 * @brief Dieses Modul geh�rt zur L�sung der GS Probeklausur WS 2016-17. 
 *        Auf Basis des TIM2 Timers stellt es einfache Funktionen 
 *        zur Behandlung von Zeit zur Verf�gung
 */
 

#include <stdint.h>
void initTimer();
/**
 * @brief Diese Funktion wartet die vorgegebene Zeitspanne.
 *        Sie ist durch polling der Timers TIM2 realisiert.
 * @param t Die Zeit in us, die gewartet werden soll. Der Wertebereich des 
 *        Parameters ist 0 .. (UINT32_MAX-1)/TIM2_CLOCK
 */
void sleep(const uint32_t t);

// EOF


/**
 * @file hal.h
 * @author Franz Korf, HAW Hamburg 
 * @date Januar 2017
 * @brief Dieses Modul geh�rt zur L�sung der GS Klausur WS 2016-17. 
 *        Es stellt  den Hardware Abstraction Layer fuer den Zugriff
 *	      auf GPIO dar.
 *        Dabei wird von ausgegangen, dass die verwendeten Ports schon in 
 *        der durch die Funktion Init_TI_Board() gemaess ihrer Funktion als
 *        Input oder Output eingestellt wurden.
 */

#ifndef _HAL_H
#define _HAL_H

#include "TI_memory_map.h"
#include <stdbool.h>

/**
 * @brief Diese Funktion setzt mehrere  Pins eines Output Ports.
 *        Ein Parameter definiert diese Pins.
 *
 * @param port Das Port, dessen Pins gesetzt werden sollen.
 * @param relevantPins Diese Maske beschreibt die Pins, die modifiziert 
 *        werden sollen.
 * @param mask Diese Maske beschreibt die Werte der zu modifizierenden Pins. 
 */

void setOutputOfPort(GPIO_TypeDef* port, uint16_t relevantPins, uint16_t mask);

/**
 * @brief Diese Funktion liest mehrere Pins eines Ports ein. An diese
 *        Pins sind Taster angeschlossen. Ein Parameter definiert diese Pins.
 *        F�r die zu lesenden Pins (und nur fuer diese Pins) 
 *        wird eine softwaremaessige Entprellung realisiert.
 *        Schlaegt die Entprellung fehl, ist das Ergebnis der 
 *        Funktion 0x00 und der entsprechende Error Code wird gesetzt.
 *         
 * @param port Das Port, dessen Pins gelesen werden sollen.
 * @param relevantPins Diese Maske beschreibt die Pins, 
 *        die gelesen werden sollen. 
 * @retval Im return Wert sind genau die Bits gesetzt, deren
 *         zugeh�riger Taster gedr�ckt ist.
 */
uint16_t readButtons(GPIO_TypeDef* port, uint16_t relevantPins);

#endif
// EOF
